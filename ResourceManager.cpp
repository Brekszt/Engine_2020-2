#include "ResourceManager.h"

ResourceManager* ResourceManager::instance = 0;


int ResourceManager::nextId()
{
	return this->actualId++;
}

ResourceManager* ResourceManager::get_Instance()
{
	if (instance == nullptr)
	{
		instance = new ResourceManager();
	}
	return instance;
}
ResourceManager::ResourceManager()
{
	this->actualId = 0;
}

ResourceManager::~ResourceManager()
{
}


Texture* ResourceManager::getTexture(std::string key)
{
	if (texture_pool[key]==nullptr)
	{
		texture_pool[key] = TextureManager::loadTexture(key.c_str());
	}
	return texture_pool[key];
}
