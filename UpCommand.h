#pragma once
#include "Command.h"
class UpCommand :public Command
{
public:
    void execute(GameObject* actor)override;
    UpCommand();
    ~UpCommand();
};