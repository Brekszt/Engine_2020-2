#pragma once
#include "Scene.h"
#include "RenderObject.h"


class InGame :public Scene
{
public:
	InGame();
	~InGame();
	void onUpdate()override;
	void onRender()override;
};
