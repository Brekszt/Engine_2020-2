#pragma once
#include <string>
#include <unordered_map>
#include "TextureManager.h"



class ResourceManager
{

private:
	int actualId;
	static ResourceManager* instance;
	ResourceManager();
	~ResourceManager();

public:
	int nextId();
	std::unordered_map<std::string, Texture*> texture_pool;
	static ResourceManager* get_Instance();
	Texture* getTexture(std::string key);
};

