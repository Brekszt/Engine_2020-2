#include "Transformation.h"

Transformation::Transformation()
{
	Owner = new SpatialObject();
	escala = Matriz();
	rotacion = Matriz();
	translacion = Matriz();
	acumulada = Matriz();
	transformaciones = Matriz();
}

Transformation::Transformation(SpatialObject* _obj)
{
	this->Owner = _obj;
	escala = Matriz();
	rotacion = Matriz();
	translacion = Matriz();
	acumulada = Matriz();
	transformaciones = Matriz();
}

void Transformation::SetMatrix()
{
	escala.Identidad();
	rotacion.Identidad();
	translacion.Identidad();
	acumulada.Identidad();
	transformaciones.Identidad();
}

void Transformation::LocalToGlobal()
{
}


Transformation::~Transformation()
{
}