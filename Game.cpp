#include "Game.h"

Game* g = 0;

void keyboard_s(unsigned char key, int x, int y)
{
    g->keyboard(key, x, y);
}

void display_s(void)
{
    g->display();
}

void reshape_s(int w, int h)
{
    g->reshape(w, h);
}

void gameLoop_s(void)
{
    g->gameLoop();
}

void Game::init()
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glShadeModel(GL_FLAT);
    glEnable(GL_DEPTH_TEST);

}

void Game::display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthMask(GL_FALSE);
    glEnable(GL_TEXTURE_2D);

    root->onRender();

    glDisable(GL_TEXTURE_2D);
    glutSwapBuffers();

}




void Game::reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei)w, (GLsizei)h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, w, 0, h, -10, 10);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}

void Game::keyboard(unsigned char key, int x, int y)
{
    switch (key) {
    case 27:
        exit(0);
        break;
    default:
        root->onKey(key);
        break;
    }
}

void Game::gameLoop()
{
    int fps = 60;
    
    auto start = std::chrono::system_clock::now();
    root->onUpdate();
    glutPostRedisplay();
    auto end = std::chrono::system_clock::now();
    std::chrono::duration<float, std::milli> duracion = end - start;
    std::cout << "Duracion= " << duracion.count() << "m\n";

}



Game::Game(int argc, char** argv, int w, int h)
{
    g = this;


    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(w, h);
    glutInitWindowPosition(600, 600);
    glutCreateWindow("Game");
    this->init();
    glutDisplayFunc(display_s);
    glutReshapeFunc(reshape_s);
    glutKeyboardFunc(keyboard_s);
    glutIdleFunc(gameLoop_s);
}

void Game::SetScene(Scene* root)
{
    this->root = root;
}

void Game::Play()
{
    glutMainLoop();
}


