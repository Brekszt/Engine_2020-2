#pragma once
#include <string>
#include "vector2D.h"
#include "ResourceManager.h"
#include "Physics.h"
#include "PhyStructures.h"
#include "Transformation.h"


using namespace std;
class Physics;
class Transformation;
class SpatialObject
{

public:
	int id;
	string name;
	vector2D position;
	vector2D escala;
	float rotacion;
	Collider collider;
	Transformation* transform;
	SpatialObject* partner;
	int w;
	int h;

	void AddCircle(float _r);
	void AddBox(float _w, float _h);
	SpatialObject();
	SpatialObject(int _id, string name,float _x,float _y,float rot);
	~SpatialObject();
	virtual void onRender();
	virtual void onUpdate();
	virtual void onKey(char key);
};