#pragma once
#include "stb_image.h"
typedef unsigned char byte;
struct Texture {
	byte* bits;
	//image width and height
	unsigned int width, height;
};
class TextureManager
{
public:

public:
	static Texture* loadTexture(const char* filename);
};

