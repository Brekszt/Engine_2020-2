#pragma once
#include "SpatialObject.h"
#include "ResourceManager.h"
#include "TextureManager.h"
#include <string>
#include <GL/glew.h>
#include <GL/glut.h>

class RenderObject : public SpatialObject
{
public:
	Texture* tex;
	GLuint texName;
	vector2D vec[4];
	RenderObject(std::string key);
	virtual void onRender()override;
	virtual void onUpdate()override;
};

