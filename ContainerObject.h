#pragma once
#include "SpatialObject.h"
#include <vector>

class ContainerObject : public SpatialObject
{
private:
	std::vector<SpatialObject*> children;
public:
	int numChildren;

	ContainerObject();
	~ContainerObject();
	int addChild(SpatialObject* object);
	int addChildAt(SpatialObject* object,int index);
	int removeChild(SpatialObject* object);
	int removeChild(int index);
	void removeChildren();
	int search(SpatialObject* object);
	SpatialObject* getChildAt(int index);
	virtual void onUpdate() override;
	virtual void onRender() override;
	virtual void onKey(char key) override;
};

