#pragma once
#include "ContainerObject.h"

class Scene : public ContainerObject
{
public:
    Scene();
    void onUpdate()override;
    void onRender()override;
    ~Scene();
};


