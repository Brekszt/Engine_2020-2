#include "InputHandler.h"

InputHandler::InputHandler()
{
	buttonA = new LeftCommand();
	buttonD = new RightCommand();
	buttonW = new UpCommand();
	buttonS = new DownCommand();
}
Command* InputHandler::handleInput(char key)
{	
	switch (key)
	{
	case 'a':
		return buttonA;
		break;
	case 'd':
		return buttonD;
		break;
	case 'w':
		return buttonW;
		break;
	case 's':
		return buttonS;
		break;
	default:
		return nullptr;
		break;
	}
}