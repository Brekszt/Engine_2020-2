#include "Physics.h"

Physics* Physics::instance = 0;

Physics* Physics::get_Instance()
{
	if (instance == nullptr)
	{
		instance = new Physics();
	}
	return instance;
}

void Physics::AddObject(SpatialObject* obj)
{
	this->list.push_back(obj);
}


Physics::Physics()
{
}

Physics::~Physics()
{
}