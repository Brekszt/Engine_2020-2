#pragma once
#include "Command.h"
class DownCommand :public Command
{
public:
    void execute(GameObject* actor)override;
    DownCommand();
    ~DownCommand();
};