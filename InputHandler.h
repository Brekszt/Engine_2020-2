#pragma once
#include "Command.h"
#include "LeftCommand.h"
#include "RightCommand.h"
#include "UpCommand.h"
#include "DownCommand.h"

class InputHandler
{
public:
	Command* buttonA;
	Command* buttonD;
	Command* buttonW;
	Command* buttonS;
public:
	InputHandler();
	Command* handleInput(char key);
};
