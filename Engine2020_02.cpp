#include <cstdio>
#include <cstdlib>
#include <vector>
#include "Engine2020_02.h"
#include <unordered_map>
#include <string>
#include "ResourceManager.h"
#include "Scene.h"
#include "Game.h"
#include "InGame.h"



int main(int argc, char** argv)
{
	Game* newgame = new Game(argc, argv,500,500);
	InGame* escena1 = new InGame();
	newgame->SetScene(escena1);	
	newgame->Play();
	return 0;
}