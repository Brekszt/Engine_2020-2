#include "RenderObject.h"

RenderObject::RenderObject(std::string key)
{
	this->tex = ResourceManager::get_Instance()->getTexture(key);
    w = this->tex->width;
    h = this->tex->height;
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glGenTextures(1, &texName);
    glBindTexture(GL_TEXTURE_2D, texName);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->tex->width,
        this->tex->height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
        this->tex->bits);
}

void RenderObject::onRender()
{
    SpatialObject::onRender();
    glBindTexture(GL_TEXTURE_2D, texName);
    glBegin(GL_QUADS);
    vec[0] = vector2D(-w / 2, -h / 2);  vec[1] = vector2D(w / 2, -h / 2);
    vec[2] = vector2D(w / 2, h / 2);vec[3] = vector2D(-w / 2, h / 2);

    /*for (size_t i = 0; i < sizeof(vec); i++)
    {
        vec[i] = transform->LocalToGlobal();
    }*/
    glTexCoord2f(0, 0);glVertex2f(vec[0].x, vec[0].y);
    glTexCoord2f(1, 0);glVertex2f(vec[1].x, vec[1].y);
    glTexCoord2f(1, 1);glVertex2f(vec[2].x, vec[2].y);
    glTexCoord2f(0, 1);glVertex2f(vec[3].x, vec[3].y);

    glEnd();
}

void RenderObject::onUpdate()
{
    SpatialObject::onUpdate();
}
