#pragma once
#include "Scene.h"
#include <GL/glew.h>
#include <GL/glut.h>
#include <chrono>
#include <iostream>
#include <windows.h>



class Game
{
private:

	Scene* root;

public:

	void init();
	void display();
	void reshape(int w, int h);
	void keyboard(unsigned char key, int x, int y);
	void gameLoop();

public:

	float w;
	float h;

	Game();
	Game(int argc, char** argv, int w,int h);
	void SetScene(Scene*);
	void Play();
};
