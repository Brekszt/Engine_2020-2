#include "ContainerObject.h"

ContainerObject::ContainerObject()
{
	this->numChildren = 0;
}

ContainerObject::~ContainerObject()
{
}

int ContainerObject::addChild(SpatialObject* object)
{
	this->children.push_back(object);
	object->partner = this;
	this->numChildren++;
	return this->numChildren;
}

int ContainerObject::addChildAt(SpatialObject* object, int index)
{
	this->children.insert(children.begin() + index,object);
	this->numChildren++;
	return this->numChildren;
}

int ContainerObject::removeChild(SpatialObject* object)
{
	for (size_t i = 0; i < (unsigned)numChildren; i++)
	{
		if (children[i]->id==object->id)
		{
			this->children.erase(children.begin() + i);
			this->numChildren--;
			return this->numChildren;
		}
	}
	return this->numChildren;
}


int ContainerObject::removeChild(int index)
{
	this->children.erase(children.begin() + index);
	this->numChildren--;
	return this->numChildren;
}

void ContainerObject::removeChildren()
{
	this->children.clear();
	this->numChildren = 0;
}

int ContainerObject::search(SpatialObject* object)
{
	for (size_t i = 0; i < (unsigned)numChildren; i++)
	{
		if (this->children[i]->id==object->id)
		{
			return i;
		}
		return -1;
	}
}

SpatialObject* ContainerObject::getChildAt(int index)
{
	return this->children[index];
}

void ContainerObject::onUpdate()
{
	SpatialObject::onUpdate();
	for (size_t i = 0; i < (unsigned)numChildren; i++)
	{
		this->children[i]->onUpdate();
	}
}

void ContainerObject::onRender()
{
	SpatialObject::onRender();
	for (size_t i = 0; i < (unsigned)numChildren; i++)
	{
		this->children[i]->onRender();
	}
}

void ContainerObject::onKey(char key)
{
	SpatialObject::onKey(key);
	for (size_t i = 0; i < (unsigned)numChildren; i++)
	{
		this->children[i]->onKey(key);
	}
}