#include "SpatialObject.h"

SpatialObject::SpatialObject(){
	this->name = "obj00";
	this->id = ResourceManager::get_Instance()->nextId();
	this->position = vector2D();
	this->escala = vector2D(1, 1);
	this->rotacion = 0;
	this->transform = new Transformation(this);
}

SpatialObject::SpatialObject(int _id, string name, float _x, float _y, float _rot)
{
	this->name = name;
	this->id = _id;
	this->position = vector2D(_x,_y);
	this->escala = vector2D(1, 1);
	this->rotacion = _rot;
}

SpatialObject::~SpatialObject()
{
}
void SpatialObject::onRender() {
}
void SpatialObject::onUpdate() {
	
}

void SpatialObject::onKey(char key)
{
}

void SpatialObject::AddCircle(float _r)
{	
	collider.tipo = 'c';
	collider.r = _r;
	Physics::get_Instance()->AddObject(this);
}
void SpatialObject::AddBox(float _w, float _h)
{
	collider.tipo = 'b';
	collider.w = _w;
	collider.h = _h;
	Physics::get_Instance()->AddObject(this);
}