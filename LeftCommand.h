#pragma once
#include "Command.h"
class LeftCommand :public Command
{
public:
    void execute(GameObject* actor)override;
    LeftCommand();
    ~LeftCommand();
};