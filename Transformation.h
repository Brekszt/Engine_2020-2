#pragma once
#include "SpatialObject.h"
#include "Matriz.h"

class SpatialObject;

class Transformation
{
public:
	Matriz translacion;
	Matriz escala;
	Matriz rotacion;
	Matriz acumulada;
	Matriz transformaciones;
public:
	SpatialObject* Owner;
	Transformation();
	~Transformation();
	Transformation(SpatialObject* _obj);
	void Identidad(Matriz _m);
	void SetMatrix();
	void LocalToGlobal();

};

