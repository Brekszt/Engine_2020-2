#define STB_IMAGE_IMPLEMENTATION

#include "TextureManager.h"

Texture* TextureManager::loadTexture(const char* filename)
{
	Texture* textura = new Texture();
	int width, height;
	textura->bits = stbi_load(filename, &width, &height, nullptr, 4);
	for (int i = 0; i < (height / 2); ++i) {
		for (int j = 0; j < (width * 4); ++j) {
			byte aux = textura->bits[i * (width * 4) + j];
			textura->bits[i * (width * 4) + j] = textura->bits[(height - (i + 1)) * (width * 4) + j];
			textura->bits[(height - (i + 1)) * (width * 4) + j] = aux;
		}
	}
	textura->width = width;
	textura->height = height;

	//return success
	return textura;
}
