#pragma once
#include "SpatialObject.h"
#include "PhyStructures.h"
#include <vector>
#include <string>

class SpatialObject;
class Physics

{
private:
	static Physics* instance;
	Physics();
	~Physics();

public:
	static Physics* get_Instance();
	std::vector<SpatialObject*> list;
	void AddObject(SpatialObject* obj);
};
