#pragma once
#include "Command.h"
class RightCommand :public Command
{
public:
    void execute(GameObject* actor)override;
    RightCommand();
    ~RightCommand();
};